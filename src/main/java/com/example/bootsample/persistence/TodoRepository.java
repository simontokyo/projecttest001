package com.example.bootsample.persistence;

import com.example.bootsample.entity.Todo;

import java.util.List;

public interface TodoRepository {
    List<Todo> findAll();

    Todo findById(long id);

    void insert(String newTodoText);

    void changeDoneTrue(long id);

    void delete(long id);
}
